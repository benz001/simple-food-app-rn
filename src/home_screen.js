import React, { useEffect, useState } from 'react';
import { View, Text, Image, ActivityIndicator, FlatList, SafeAreaView, TouchableOpacity } from 'react-native';
import axios from 'axios';

export default function HomeScreen({ navigation }) {
    const [isLoading, setIsLoading] = useState(true);
    const [listFood, setListFood] = useState([]);

    useEffect(() => {
        _fetchFoods();
    }, [])


    const _fetchFoods = async () => {
        try {
            const result = await axios.get('http://3.15.16.42:7000/foods');
            console.log(result.data);
            setIsLoading(false);
            setListFood(result.data);
        } catch (error) {
            console.log(error);
            alert(error.toString());
        }
    }

    const _itemComponent = ({ item }) => {
        return (
            <TouchableOpacity
                onPress={() => navigation.navigate('Food Detail', {
                    name: item.name,
                    image: item.image,
                    desc: item.desc
                })}
                style={{
                    width: '50%',
                    height: 180,
                    flexDirection: 'column',
                    marginLeft: 5,
                    backgroundColor: 'white',
                    marginTop: 10, borderColor: 'lightgrey',
                    borderWidth: 1,
                    overflow: 'hidden',
                    shadowColor: 'lightgrey',
                    shadowRadius: 10,
                    shadowOpacity: 1,
                }} >
                <View style={{ height: '70%', justifyContent: 'center', backgroundColor: 'lightgrey' }}>
                    <Image source={{ uri: item.image }}
                        style={{ flex: 1 }}
                        resizeMode='cover'
                    />
                </View>
                <View style={{ flex: 2, height: 50 }}>
                    <Text style={{ padding: 5, textAlign: 'center' }} >{item.name}</Text>
                </View>
            </TouchableOpacity>
        )
    }

    if (isLoading) {
        return (
            <View style={{ width: '100%', height: '100%', backgroundColor: 'lightgrey', justifyContent: 'center' }}>
                <ActivityIndicator
                    animating={true}
                    color='blue'
                    size='large'
                />
            </View>
        )
    }
    return (
        <SafeAreaView>
            <View style={{ padding: 5 }}>
                <FlatList
                    data={listFood}
                    renderItem={_itemComponent}
                    keyExtractor={(item, index) => index.toString()}
                    refreshing={isLoading}
                    numColumns={2}
                />
            </View>
        </SafeAreaView>
    );
}
