import React from 'react';
import { View, Image, Text } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';

export default function DetailScreen({ route }) {
    const { name, image, desc } = route.params
    return (
        <SafeAreaView>
            <View style={{ width: '100%', height: '100%', flexDirection: 'column' }}>
                <View style={{ width: '100%', height: '100%', flexDirection: 'column' }}>
                    <View style={{ width: '100%', height: '30%', backgroundColor: 'lightgrey' }}>
                        <Image source={{ uri: image }}
                            style={{ flex: 1 }}
                            resizeMode='cover'
                        />
                    </View>
                    <View style={{ width: '100%', height: '70%', backgroundColor: 'white', paddingStart: 15, paddingEnd: 15, flexDirection: 'column' }}>
                        <Text style={{ textAlign: 'center', fontSize: 18, fontWeight: 'bold', textDecorationLine: 'underline' }}>{name}</Text>
                        <Text style={{ fontSize: 14, fontWeight: 'bold', fontStyle: 'italic' }}>How to :</Text>
                        <Text style={{ fontSize: 12, }}>{desc}</Text>
                    </View>

                </View>
            </View>
        </SafeAreaView>
    );
}