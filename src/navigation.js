import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from './home_screen';
import DetailScreen from './detail_screen';

const Stack = createStackNavigator();


export default function Navigation(){
    return(
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen name='List Foods' component={HomeScreen}/>
                <Stack.Screen name='Food Detail' component={DetailScreen}/>
            </Stack.Navigator>
        </NavigationContainer>
    );
}